#!/usr/bin/env bash

date=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
body='{ "job": "test", "service":"test-service", "hostname":"test-instance", "source": "fluentd", "severity": "info" , "timestamp":"'"$date"'", "message":"Test message" }'

curl \
    -v \
    -X POST "http://localhost:8888/test-service.log" \
    -H "Content-Type: application/json" \
    --data-raw "$body"

podman logs fluentd
