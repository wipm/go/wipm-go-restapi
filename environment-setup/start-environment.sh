#!/bin/bash

source ./set-environment.sh

./create-network.sh
./start-loki.sh
./start-fluentd.sh
./start-grafana.sh

