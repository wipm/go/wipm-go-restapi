#!/bin/bash

service='fluentd'

if ! podman ps --format '{{.Names}}' | grep -q "$service"; then

    podman run \
        -d \
        -it \
        --rm \
        -v "$(pwd)/fluentd-config.conf:/fluentd/etc/fluent.conf" \
        --network="$NETWORK_NAME" \
        -p 24224:24224 \
        -p 8888:8888 \
        --name $service \
        grafana/fluent-plugin-loki:master

        # -p 24224:24224 \
        # -p 24224:24224/udp \
fi
