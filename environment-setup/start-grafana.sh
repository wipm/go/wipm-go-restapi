#!/bin/bash

service='grafana'

if ! podman ps --format '{{.Names}}' | grep -q $service; then

    podman run \
        -d \
        -it \
        --rm \
        --network="$NETWORK_NAME" \
        -p 3000:3000 \
        --name $service \
        grafana/grafana:latest

        # -p 3000:3000 
fi
