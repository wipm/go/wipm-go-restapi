#!/usr/bin/env bash

date=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
body='{"streams": [{ "stream": { "source": "loki", "job": "test", "severity": "info"}, "values": [[ "'$(date +%s%N)'", "Test message" ]]}]}'

curl \
    -v \
    -XPOST "http://localhost:3100/loki/api/v1/push" \
    -H "Content-Type: application/json" \
    --data-raw "$body"

curl -X GET "http://localhost:3100/loki/api/v1/labels" 
