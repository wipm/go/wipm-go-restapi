#!/bin/bash

service='loki'

if ! podman ps --format '{{.Names}}' | grep -q $service; then

    podman run \
        -d \
        --rm \
        -it \
        --network="$NETWORK_NAME" \
        -p 3100:3100 \
        --name $service \
        grafana/loki:latest 

    # -p 3100:3100

fi
