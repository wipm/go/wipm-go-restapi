#!/bin/bash

echo "Teardown environment"

source ./set-environment.sh

services=(
    "grafana"
    "otel-collector"
    "fluentd"
    "loki"
)

for s in "${services[@]}"; do

    if podman ps --format "{{.Names}}" | grep -q "$s"; then
        podman stop "$s"
    fi

done

podman network rm "$NETWORK_NAME"
