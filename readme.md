# Example web api written in go

Reference example go webapi that includes the basic features needed in webapi such as telemety, config, security etc.


## Installation/Runing

The services is a single binary that start a listening for http requests, interupt Ctrl+C and SIGTEM will shut the service down gracefully.  There are various configuration settings that can be set for the service which will be loaded from one of a number of sources, commandline, environment variable, configuration file, or hardcoded default, a setting is loaded from the first valid sources with the precedence being commandline, environment variable, configuration file, hardcoded default  The service supports setting the following configuration values:

- ReadTimeOutInSeconds:  "10",
  - Number of seconds allow for a connection to send a complete request before timming out
  - Command line swith   : -r, -read-timeout-secs
  - Environment variable : READ_TIMEOUT_IN_SECONDS
  - Config setting       : read_timeout_in_seconds

- WriteTimeOutInSeconds: "10",
  - Number of seconds allowed for response before timming out
  - Command line switch  : -w -write-timeout-secs
  - Environment variable : WRITE_TIMEOUT_IN_SECONDS
  - Config setting       : write_timeout_in_seconds

- ListenAddress:         "localhost",
  - IP address the service listens for connections on
  - command line switch  : -a, -listen-address
  - Environment variable : LISTEN_ADDRESS
  - Config setting       : listen_address

- ListenPort:            "8080",
  - Port the service listens for connections on
  - Command line switch  : -p, -listen-port
  - Environment variable : LISTEN_PORT
  - Config setting       : listen_port

- LogToFlunetd:          "n"
  - Write log messages to a fluentd log shipper
  - Command line switch : -log-to-fluentd
  - Environment variable : LOG_TO_FLUENTD ( "y", "yes" to switch on case insensitive )
  - Config setting       : log_to_fluentd ( "y", "yes" to switch on case insensitive )

- FlunetdHost
  - Host name of the fluentd log shipper, required if logging to fluentd
  - Command line switch  : -fluentd-host
  - Environment variable : FLUENTD_HOST
  - Config setting       : fluentd_host

- FluentdPort
  - Port the fluentd log shipper listens on, required if logging to flunentd
  - Command line switch  : -fluentd-port
  - Environment variable : FLUENTD_PORT
  - Config setting       : fluentd_port

- FluentdLog
  - log name to write fluentd logs to, if not set defaults to the sevice name
  - Command line switch  : -fluentd-log
  - Environment variable : FLUENTD_LOG
  - Config setting       : fluentd_log

- FluentdNumBatches      "100"
  - Number of batches that are buffered before log messages are droped
  - Command line switch  : -fluentd-num-batches
  - Environment variable : FLUENTD_NUM_BATCHES
  - Config setting       : fluentd_num_batches

- FluentdBatchSize       "10"
  - Number of messages that are sent to fluentd in one requests
    - n. partial batches will be sent if ready to send before a batch has been filled
  - Command line switch  : -fluentd-batch-size
  - Environment variable : FLUENTD_BATCH_SIZE
  - Config setting       : fluentd_batch_size

- FluentdBackoffDurationInSeconds   "2"
  - Period to wait before atempt to resend logs after failed attempt
    - n. the backoff period is multiplied by the number of retries
  - Command line switch  : -fluentd-backoff-duration-secs
  - Environment variable : FLUENTD_BACKOFF_DURATION_IN_SECONDS
  - Config setting       : fluentd_backoff_duration_in_seconds

- FluentdMaxRetries    "5"
  - Number of times sending logs should be tried before being dropped
  - Command line switch  : fluentd-max-retries 
  - Environment variable : FLUENTD_MAX_RETRIES
  - Config setting       : fluentd_max_retries

## Demonstrated Functionality

- Shutdown on signal
- Configuration
