package infra

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// Setting that can be supplied at runtime
type Settings struct {
	ReadTimeOutInSeconds            string `json:"read_timeout_in_seconds"`
	WriteTimeOutInSeconds           string `json:"write_timeout_in_seconds"`
	ListenAddress                   string `json:"listen_address"`
	ListenPort                      string `json:"listen_port"`
	LogToFluentd                    string `json:"log_to_fluentd"`
	FluentdHost                     string `json:"fluentd_host"`
	FluentdPort                     string `json:"fluentd_port"`
	FluentdLog                      string `json:"fluentd_tag"`
	FluentdBatchSize                string `json:"fluentd_batch_size"`
	FluentdNumBatches               string `json:"fluentd_num_batches"`
	FluentdBackoffDurationInSeconds string `json:"fluentd_backoff_duration_in_seconds"`
	FluentdMaxRetries               string `json:"fluentd_max_retries"`
}

// Server configuration
type Config struct {
	ReadTimeOutInSeconds            int
	WriteTimeOutInSeconds           int
	ListenAddress                   string
	LogToFluentd                    bool
	FluentdAddress                  string
	FluentdLog                      string
	FluentdBatchSize                int
	FluentdNumBatches               int
	FluentdBackoffDurationInSeconds int
	FluentdMaxRetries               int
}

func GetSettingsFromCommandLine() Settings {
	var settings Settings

	flag.StringVar(&settings.ReadTimeOutInSeconds, "read-timeout-secs", "", "Number of seconds allowed to recieving the entire requests body before before timing out")
	flag.StringVar(&settings.ReadTimeOutInSeconds, "r", "", "Number of seconds allowed to recieving the entire requests body before before timing out")

	flag.StringVar(&settings.WriteTimeOutInSeconds, "write-timeout-secs", "", "Number of seconds allow to respond to a request before timing out")
	flag.StringVar(&settings.WriteTimeOutInSeconds, "w", "", "Number of seconds allow to respond to a request before timing out")

	flag.StringVar(&settings.ListenAddress, "listen-address", "", "Address that the server should listen on")
	flag.StringVar(&settings.ListenAddress, "a", "", "Address that the server should listen on")

	flag.StringVar(&settings.ListenPort, "listen-port", "", "Port that the service should listen on")
	flag.StringVar(&settings.ListenPort, "p", "", "Port that the service should listen on")

	logToFluentd := flag.Bool("log-to-fluentd", false, "Log to fluentd")

	flag.StringVar(&settings.FluentdHost, "fluentd-host", "", "Fluentd host")
	flag.StringVar(&settings.FluentdPort, "fluentd-port", "", "Fluentd port")
	flag.StringVar(&settings.FluentdLog, "fluentd-log", "", "Fluentd log")
	flag.StringVar(&settings.FluentdBatchSize, "fluentd-batch-size", "", "Number of messages to be sent to fluentd in a request")
	flag.StringVar(&settings.FluentdNumBatches, "fluentd-num-batches", "", "Number of batches that can be buffered before messages are droped")
	flag.StringVar(&settings.FluentdBackoffDurationInSeconds, "fluentd-backoff-duration-secs", "", "Duration to wait befor retrying to send logs to fluentd")
	flag.StringVar(&settings.FluentdMaxRetries, "fluentd-max-retries", "", "Maximum number of attempts to send logs to fluentd")

	flag.Parse()

	if *logToFluentd {
		settings.LogToFluentd = "y"
	} else {
		settings.LogToFluentd = "n"
	}

	return settings
}

func GetSettingsFromEnvironmentVariables() Settings {
	return Settings{
		ReadTimeOutInSeconds:            os.Getenv("READ_TIMEOUT_IN_SECONDS"),
		WriteTimeOutInSeconds:           os.Getenv("WRITE_TIMEOUT_IN_SECONDS"),
		ListenAddress:                   os.Getenv("LISTEN_ADDRESS"),
		ListenPort:                      os.Getenv("LISTEN_PORT"),
		LogToFluentd:                    os.Getenv("LOG_TO_FLUENTD"),
		FluentdHost:                     os.Getenv("FLUENTD_HOST"),
		FluentdPort:                     os.Getenv("FLUENTD_PORT"),
		FluentdLog:                      os.Getenv("FLUENTD_LOG"),
		FluentdBatchSize:                os.Getenv("FLUENTD_BATCH_SIZE"),
		FluentdNumBatches:               os.Getenv("FLUENTD_NUM_BATCHES"),
		FluentdBackoffDurationInSeconds: os.Getenv("FLUENTD_BACKOFF_DURATION_IN_SECONDS"),
		FluentdMaxRetries:               os.Getenv("FLUENTD_MAX_RETRIES"),
	}
}

func GetConfigFilePath() string {
	filePath := ""

	filePath = os.Getenv("CONFIG_FILE_PATH")
	if filePath != "" {
		println("using config file path from environment variable")
		return filePath
	}

	return "./config.json"
}

func GetSettingFromJsonConfigFile(configFilePath string) Settings {
	var settings Settings

	file, err := os.Open(configFilePath)
	if err != nil {
		fmt.Println("Error opening config file [", configFilePath, "]:", err)
		return settings
	}
	defer file.Close()

	fileStat, err := file.Stat()
	if err != nil {
		fmt.Println("Error getting file size [", configFilePath, "]:", err)
		return settings
	}

	jsonData := make([]byte, fileStat.Size())
	_, err = file.Read(jsonData)
	if err != nil {
		fmt.Println("Error reading config from file [", configFilePath, "]:", err)
		return settings
	}

	err = json.Unmarshal(jsonData, &settings)
	if err != nil {
		fmt.Println("Error umarshaling config data into settings [", configFilePath, "]:", err)
		return Settings{}
	}
	return settings
}

func GetDefaultSettings() Settings {
	return Settings{
		ReadTimeOutInSeconds:            "10",
		WriteTimeOutInSeconds:           "10",
		ListenAddress:                   "localhost",
		ListenPort:                      "8080",
		LogToFluentd:                    "n",
		FluentdBatchSize:                "10",
		FluentdNumBatches:               "100",
		FluentdBackoffDurationInSeconds: "2",
		FluentdMaxRetries:               "5",
	}
}

type SettingValue func(from Settings) string
type IntValidator func(int) error
type StringValidator func(string) error
type StringFlag func(string) bool

// / Use when there is no specific validation required
func IntIsValid(int) error {
	return nil
}

func GetIntSetting(name string, errorValue int, get SettingValue, validate IntValidator, settings []Settings) (int, error) {
	for _, settingsSource := range settings {
		setting := get(settingsSource)

		if setting != "" {
			setting, err := strconv.Atoi(setting)
			if err != nil {
				return errorValue, fmt.Errorf("%s: %w", name, err)
			}
			err = validate(setting)
			if err != nil {
				return errorValue, fmt.Errorf("%s: %w", name, err)
			}
			return setting, nil
		}
	}
	return errorValue, fmt.Errorf("%s: Setting not specified", name)
}

// / Use when there is no specific validation required
func StringIsValid(string) error {
	return nil
}

func GetStringSetting(name string, errorValue string, get SettingValue, validate StringValidator, settings []Settings) (string, error) {
	for _, settingsSource := range settings {
		setting := get(settingsSource)

		if setting != "" {
			err := validate(setting)
			if err != nil {
				return errorValue, fmt.Errorf("%s: %w", name, err)
			}
			return setting, nil
		}
	}
	return errorValue, fmt.Errorf("%s: Setting not specified", name)
}

func GetStringFlag(name string, get SettingValue, validate StringFlag, settings []Settings) (bool, error) {
	for _, settingsSource := range settings {
		setting := get(settingsSource)

		if setting != "" {
			flag := validate(setting)
			return flag, nil
		}
	}
	return false, fmt.Errorf("%s: Setting not specified", name)
}

func IsValidPort(portNumber int) error {

	if portNumber < 0 || portNumber > 65535 {
		return fmt.Errorf("%d invalid portnumber, valid ports are 0..65535", portNumber)
	}
	return nil
}

func ResolveConfig(settings []Settings, serviceName string) (Config, []error) {
	errs := []error{}

	readTimeOutInSeconds, err := GetIntSetting("readTimeOutInSeconds", 0, func(from Settings) string { return from.ReadTimeOutInSeconds }, IntIsValid, settings)
	if err != nil {
		errs = append(errs, err)
	}

	writeTimeOutInSeconds, err := GetIntSetting("writeTimeOutInSeconds", 0, func(from Settings) string { return from.WriteTimeOutInSeconds }, IntIsValid, settings)
	if err != nil {
		errs = append(errs, err)
	}

	listenAddress, err := GetStringSetting("listenAddress", "", func(from Settings) string { return from.ListenAddress }, StringIsValid, settings)
	if err != nil {
		errs = append(errs, err)
	}

	listenPort, err := GetIntSetting("listenPort", 0, func(from Settings) string { return from.ListenPort }, IsValidPort, settings)
	if err != nil {
		errs = append(errs, err)
	}

	logToFluentd, err := GetStringFlag(
		"logToFluentd",
		func(from Settings) string { return from.LogToFluentd },
		func(from string) bool { return strings.ToLower(from) == "y" || strings.ToLower(from) == "yes" },
		settings,
	)
	if err != nil {
		errs = append(errs, err)
	}

	fluentdHost, err := GetStringSetting("fluentdHost", "", func(from Settings) string { return from.FluentdHost }, StringIsValid, settings)
	if err != nil && logToFluentd {
		errs = append(errs, err)
	}

	fluentdPort, err := GetIntSetting("fluentdPort", 0, func(from Settings) string { return from.FluentdPort }, IsValidPort, settings)
	if err != nil && logToFluentd {
		errs = append(errs, err)
	}

	fluentdLog, err := GetStringSetting("fluentdLog", "", func(from Settings) string { return from.FluentdLog }, StringIsValid, settings)
	if err != nil && logToFluentd {
		fluentdLog = serviceName + ".log"
	}

	fluentdBatchSize, err := GetIntSetting("fluentdBatchSize", 0, func(from Settings) string { return from.FluentdBatchSize }, IntIsValid, settings)
	if err != nil && logToFluentd {
		errs = append(errs, err)
	}

	fluentdNumBatches, err := GetIntSetting("fluentdNumBatches", 0, func(from Settings) string { return from.FluentdNumBatches }, IntIsValid, settings)
	if err != nil && logToFluentd {
		errs = append(errs, err)
	}

	fluentdLogBackoffDurationInSeconds, err := GetIntSetting("fluentdBackoffDurationInSeconds", 0, func(from Settings) string { return from.FluentdBackoffDurationInSeconds }, IntIsValid, settings)
	if err != nil && logToFluentd {
		errs = append(errs, err)
	}

	fluentdMaxRetries, err := GetIntSetting("fluentdMaxRetries", 0, func(from Settings) string { return from.FluentdMaxRetries }, IntIsValid, settings)
	if err != nil && logToFluentd {
		errs = append(errs, err)
	}

	config := Config{
		ReadTimeOutInSeconds:            readTimeOutInSeconds,
		WriteTimeOutInSeconds:           writeTimeOutInSeconds,
		ListenAddress:                   listenAddress + ":" + strconv.Itoa(listenPort),
		LogToFluentd:                    logToFluentd,
		FluentdAddress:                  fluentdHost + ":" + strconv.Itoa(fluentdPort),
		FluentdLog:                      fluentdLog,
		FluentdBatchSize:                fluentdBatchSize,
		FluentdNumBatches:               fluentdNumBatches,
		FluentdBackoffDurationInSeconds: fluentdLogBackoffDurationInSeconds,
		FluentdMaxRetries:               fluentdMaxRetries,
	}

	if len(errs) > 0 {
		return config, errs
	}

	return config, nil
}
