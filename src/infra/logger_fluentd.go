package infra

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"
)

type FluentdConfig struct {
	HostUrl                  string
	BatchSize                int
	NumBatches               int
	BackoffDurationInSeconds int
	MaxRetries               int
}

type FluentdLogger struct {
	config FluentdConfig
	in     chan<- map[string]string
	out    <-chan []map[string]string
	read   <-chan struct{}
}

func NewFluentdLogger(
	config FluentdConfig,
) *FluentdLogger {

	in := make(chan map[string]string, 100)
	out := make(chan []map[string]string)
	read := make(chan struct{})

	// exporter
	go func() {
		for {
			read <- struct{}{}
			println("exporter: requested batch")
			batch := <-out
			println("exporer: recieved batch")
			println("exporter.batch_size: ", len(batch))

			num_tries := 0
			for {

				if sendBatchToFluentdHttp(config.HostUrl, batch) {
					break
				}

				num_tries++
				if num_tries == config.MaxRetries {
					println("Fluentd logger: max tries exceded")
					return
				}

				sleep_duration := config.BackoffDurationInSeconds * num_tries
				time.Sleep(time.Duration(sleep_duration) * time.Second)
			}
		}
	}()

	// dispatcher
	go func() {
		buffer := newBatchedRingBuffer(config.NumBatches, config.BatchSize)
		batch_requested_by_exporter := false

		for {
			select {
			case message := <-in:
				println("dispatcher: recieved message")

				// make space for new message if we can
				if buffer.isFull() && batch_requested_by_exporter {
					batch, gotBatch := buffer.read()
					if gotBatch {
						println("dispatcher(1): got batch")
						out <- batch
						println("dispatcher(1): sent batch")
						batch_requested_by_exporter = false
					}
				}

				// write message to ring buffer, drop if full
				buffer.write(message)

				// write to exporter if read requested
				if batch_requested_by_exporter {
					batch, gotBatch := buffer.read()
					if gotBatch {
						println("dispatcher(2): got batch")
						out <- batch
						println("dispatcher(2): sent batch")
						batch_requested_by_exporter = false
					}
				}

			case <-read:
				println("dispatcher: batch requested")
				batch_requested_by_exporter = true

				batch, gotBatch := buffer.read()
				if gotBatch {
					println("dispatcher(3): got batch")
					out <- batch
					println("dispatcher(3): sent batch")
					batch_requested_by_exporter = false
				}
			}
		}
	}()

	return &FluentdLogger{
		config: config,
		in:     in,
		out:    out,
		read:   read,
	}
}

func sendBatchToFluentdHttp(hostUrl string, batch []map[string]string) bool {
	jsonData, _ := json.Marshal(batch)
	println(string(jsonData))

	req, err := http.NewRequest("POST", hostUrl, bytes.NewBuffer(jsonData))
	if err != nil {
		println("error creating request for sending batch to fluentd")
		return false
	}

	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		println("error sending batch to fluentd")
		return false
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 300 {
		println("Error returned from fluentd")
		return false
	}
	return true
}

// need to change the logger to return a status
func (l *FluentdLogger) GetLogger() func(map[string]string) {

	return func(message map[string]string) {
		l.in <- message
	}
}

type batchedRingBuffer struct {
	buffer      []map[string]string
	batch_size  int
	capacity    int
	write_index int
	read_index  int
}

func newBatchedRingBuffer(
	num_batches int,
	batch_size int,
) *batchedRingBuffer {

	capacity := num_batches * batch_size

	return &batchedRingBuffer{
		buffer:      make([]map[string]string, capacity),
		batch_size:  batch_size,
		capacity:    capacity,
		write_index: 0,
		read_index:  0,
	}
}

func (rb *batchedRingBuffer) isFull() bool {
	return (rb.write_index+1)%rb.capacity == rb.read_index
}

func (rb *batchedRingBuffer) write(message map[string]string) bool {

	if (rb.write_index+1)%rb.capacity == rb.read_index {
		return false // drop when full
	}

	rb.buffer[rb.write_index] = message
	rb.write_index = (rb.write_index + 1) % rb.capacity
	return true
}

func (rb *batchedRingBuffer) read() ([]map[string]string, bool) {

	if rb.write_index == rb.read_index {
		return nil, false // empty
	}

	slice_start_index := rb.read_index
	slice_size := (rb.write_index - rb.read_index + rb.capacity) % rb.capacity

	rb.read_index = rb.read_index + rb.batch_size
	if slice_size < rb.batch_size {
		rb.write_index = rb.read_index
	}

	if slice_size > rb.batch_size {
		slice_size = rb.batch_size
	}

	batch := rb.buffer[slice_start_index : slice_start_index+slice_size]

	println("buffer.read.start_index", slice_start_index)
	println("buffer.read.batch_size", len(batch))
	return batch, true
}
