package infra

import (
	"time"
)

/*

   service   : enriched by the log exporter
   instance  : enriched by the log exporter
   time      : set by the actor writing the message
   severity  : set by the actor writing the message
   message   : set by the actor writing the message
   traceid   : set by the actor writing the message  ; middleware that enriches the log
   parentid  : set by the actor writing the message  ; middleware that enriches the log
   spanid    : set by the actor writing the message  ; middleware that enriches the log

*/

func LogMessage(severity string, message string) map[string]string {
	attributes := map[string]string{}
	attributes["severity"] = severity
	attributes["message"] = message
	attributes["recorded_at"] = time.Now().Format(time.RFC3339) // q. is the is the correct format for the timestamp
	return attributes
}

// The service will crash as a result of the issue reported in the message, Note may never get there
func FatalMessage(message string) map[string]string {
	return LogMessage("fatal", message)
}

// There was an error that we could not recover from, but the service is still running. More a request level error
func ErrorMessage(message string) map[string]string {
	return LogMessage("error", message)
}

// There was was an issue but we could continue
func WarningMessage(message string) map[string]string {
	return LogMessage("warning", message)
}

// Usefull general level information
func InfoMessage(message string) map[string]string {
	return LogMessage("info", message)
}

// Usefull for debugging
func DebugMessage(message string) map[string]string {
	return LogMessage("debug", message)
}

// Usefull for debugging, detailed information
func TraceMessage(message string) map[string]string {
	return LogMessage("trace", message)
}

// Sends logs to a log shipper (e.g. fluentd, logstash)
type LogExporter struct {
	messages chan map[string]string
	done     chan struct{}
	// t. change the writers to accept a slice
	writers   []func(map[string]string)
	enrichers []func(map[string]string)
}

func NewLogExporter(
	writers []func(map[string]string),
	enrichers []func(map[string]string),
) *LogExporter {

	return &LogExporter{
		messages:  make(chan map[string]string),
		done:      make(chan struct{}),
		writers:   writers,
		enrichers: enrichers,
	}
}

func (l *LogExporter) consume() {
	for {
		select {
		case <-l.done:
			return
		case message := <-l.messages:

			for _, enricher := range l.enrichers {
				enricher(message)
			}

			for _, writer := range l.writers {
				writer(message)
			}
		}
	}
}

func (l *LogExporter) Start() {
	// t. just do this inline, not sure the consume method is needed
	// t. need to worry about if start has already been called
	go l.consume()
}

func (l *LogExporter) Stop() {
	close(l.done)
}

// Provide a function for writing messages into the log exporter
func (l *LogExporter) GetLogger() func(map[string]string) {

	return func(message map[string]string) {
		l.messages <- message
	}
}
