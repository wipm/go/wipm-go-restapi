package main

import (
	"context"
	"example/restapi/infra"
	"fmt"
	"github.com/google/uuid"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

const service_name = "environment-management-api"

var hostname string = getHostname()

func getHostname() string {
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}
	return hostname
}

func logToConsole(message map[string]string) {

	maxLength := 0
	for key := range message {
		if len(key) > maxLength {
			maxLength = len(key)
		}
	}

	for key, value := range message {
		padding := maxLength - len(key)
		format := fmt.Sprintf("%%s%s : %%s", strings.Repeat(" ", padding))
		fmt.Printf(format, key, value)
		fmt.Println()
	}
}

func enrichLogMessages(message map[string]string) {
	message["service"] = service_name
	message["instance"] = hostname
	message["id"] = uuid.New().String()
}

// requestHandler is a simple HTTP hanler that always returns "Hello, World!"
func requestHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello, World!"))
	})
}

// Start server listening on port 8080
//
// Server will listen for
func main() {
	// Handle signals gracefully
	ctx, stop := signal.NotifyContext(
		context.Background(),
		os.Interrupt,
		syscall.SIGTERM,
	)
	defer stop()

	// Get configuration
	settings := []infra.Settings{
		infra.GetSettingsFromCommandLine(),
		infra.GetSettingsFromEnvironmentVariables(),
		infra.GetSettingFromJsonConfigFile(infra.GetConfigFilePath()),
		infra.GetDefaultSettings(),
	}
	config, errs := infra.ResolveConfig(settings, service_name)
	if errs != nil {
		println(infra.FatalMessage(fmt.Sprintf("Error getting config: %s", errs)))
		os.Exit(1)
	}

	// Setup logging
	loggers := []func(map[string]string){}

	if config.LogToFluentd {
		fluentdLogger := infra.NewFluentdLogger(infra.FluentdConfig{
			HostUrl:                  fmt.Sprintf("http://%s/%s", config.FluentdAddress, config.FluentdLog),
			BatchSize:                config.FluentdBatchSize,
			NumBatches:               config.FluentdNumBatches,
			MaxRetries:               config.FluentdMaxRetries,
			BackoffDurationInSeconds: config.FluentdBackoffDurationInSeconds,
		})
		logToFluentd := fluentdLogger.GetLogger()
		loggers = append(loggers, logToFluentd)
	} else {
		loggers = append(loggers, logToConsole)
	}

	enrichers := []func(map[string]string){
		enrichLogMessages,
	}
	logExporter := infra.NewLogExporter(loggers, enrichers)
	logExporter.Start()
	log := logExporter.GetLogger()

	// start the server
	println("Starting server on :", config.ListenAddress)
	log(infra.InfoMessage(fmt.Sprintf("Starting server on : %s", config.ListenAddress)))
	srv := &http.Server{
		Addr:         config.ListenAddress,
		Handler:      requestHandler(),
		ReadTimeout:  time.Duration(config.ReadTimeOutInSeconds) * time.Second,
		WriteTimeout: time.Duration(config.WriteTimeOutInSeconds) * time.Second,
	}
	srvErr := make(chan error, 1)
	go func() {
		srvErr <- srv.ListenAndServe()
	}()

	// wait for interuption
	select {
	case <-srvErr:
		// Error starting server so just exit
		return
	case <-ctx.Done():
		// Signal recieved
		stop()
	}

	// handle gracefull shutdown, db connections etc
	srv.Shutdown(context.Background())
	return
}
